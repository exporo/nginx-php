# nginx-php

The purpose of this project is to provide a pre-build base image for the needs when transitioning from monolith to smaller containerized services.

We deliberately break with the _SRP_ and combine _NGINX_, _custom PHP(-FPM)_, _AWS-CLI_ all in one image on top of alpine.



Building the image - and tagging it for local/further handling
```BASH
docker build -t nginx-php .
```

# Life cycle commands for building and pushing to registry 

```BASH
docker login registry.gitlab.com
```

```BASH
REGISTRY=registry.gitlab.com && \
NAMESPACE=exporo && \
IMAGE=nginx-php && \
TAG=8.0.7 && \
SIMPLE_TAG=8.0

docker build -t ${REGISTRY}/${NAMESPACE}/${IMAGE}:latest . && \
docker tag ${REGISTRY}/${NAMESPACE}/${IMAGE}:latest ${REGISTRY}/${NAMESPACE}/${IMAGE}:${TAG} && \
docker tag ${REGISTRY}/${NAMESPACE}/${IMAGE}:latest ${REGISTRY}/${NAMESPACE}/${IMAGE}:${SIMPLE_TAG} && \
docker push ${REGISTRY}/${NAMESPACE}/${IMAGE}
```
